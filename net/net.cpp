#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "net.h"
#include "table.h"
#include <stdarg.h>

#define TRAINING_SPEED 0.01 
#define TRAINING_SPEED_DELTA 0.001 
#define MAX_ITER 50

double layer_0[SIZE_LAYER_0];
double layer_1[SIZE_LAYER_1][SIZE_LAYER_1];
double weight[SIZE_LAYER_1][SIZE_LAYER_1][SIZE_LAYER_0];

table_t* top=NULL;

void weight_init()
{
	int i,j,k;

	srand((unsigned int)time(NULL));
	for (i=0; i<SIZE_LAYER_1; i++)
	{
		for (j=0; j<SIZE_LAYER_1; j++)
		{
			for (k=0; k<SIZE_LAYER_0; k++)
			{
				weight[i][j][k] = (double)(sin(rand())/20.0); 
			}
		}
	};
}

double euclid_dist(int x, int y)
{
	double dist=0.0;
	int i;

	for (i=0; i<SIZE_LAYER_0; i++)
	{
		dist += (layer_0[i]-weight[x][y][i])*(layer_0[i]-weight[x][y][i]);
	}

	return (double)(sqrt(dist));
}


double find_min_dist(int *min_x, int *min_y)
{
	double min_dist;
	double dist;
	int x; 
	int y;

	*min_x=0;
	*min_y=0;
	min_dist = euclid_dist(0,0);

	for (x=0 ; x<SIZE_LAYER_1 ; x++)
	{
		for (y=0 ; y<SIZE_LAYER_1 ; y++)
		{
			dist = euclid_dist(x,y);
			if (min_dist > dist)
			{
				min_dist=dist;
				*min_x=x;
				*min_y=y;
			}
		}
	}

	return min_dist;
}

int pattern_load(int pattern_number)
{
	table_t* record;

	record = table_get_record(top, pattern_number);

	if (record == NULL)
	{ 
		fprintf(stderr,"Table position is invalid\n");
		return -1;
	}

	layer_0[0]=record->x;
	layer_0[1]=record->y;

	return 0;
}

void run()
{
	int x, y, c, patterns_count;

	memset(layer_1, 0, SIZE_LAYER_1*SIZE_LAYER_1);
	patterns_count = table_get_count(top);
	for (int i=0 ; i<patterns_count ; i++)
	{
		if ((c=pattern_load(i)) < 0)
		{
			fprintf(stderr,"Pattern loading error\n");
			return;
		}

		find_min_dist(&x,&y);
		layer_1[x][y]=(double)(1);

		fprintf(stdout, "%03d:%d %d\n", x*SIZE_LAYER_1 + (y+1), (int)layer_0[0], (int)layer_0[1]);
	}
}

double training_speed(int t)
{
	double r;

	r = (double)(TRAINING_SPEED-(t*TRAINING_SPEED_DELTA));
	return (r>TRAINING_SPEED_DELTA)? r : TRAINING_SPEED_DELTA;
}

double gauss_neighborhood(int cx,int cy, int x, int y, int n)
{
	double d, g;
	int t=1000;

	g = (double)n / (double)t;
	g = exp(-g);
	d = ((cx-x)*(cx-x))+((cy-y) * (cy-y));
	d = exp(-((d*d)/(2.0*g*g))) * 10.0;

	return d;
}

void weight_change(int t)
{
	double dist;
	int cx,cy;
	int x,y,i,n;

	dist=find_min_dist(&cx, &cy);

	for(n=1,x=0; x<SIZE_LAYER_1; x++)
	{
		for(y=0; y<SIZE_LAYER_1; y++)
		{
			for(i=0; i<SIZE_LAYER_0 ; i++)
			{
				weight[x][y][i] += training_speed(t) * gauss_neighborhood(cx,cy,x,y,n++) * (layer_0[i] - weight[x][y][i]);
			}
		}
	}
}

int training()
{
	int files_count=0;
	int file_number=0;
	int iter=0; 

	files_count = table_get_count(top);
	weight_init();
	
	for (iter=0; iter<files_count*50; iter++)
	{
		file_number = (int)(((float)rand()/RAND_MAX)*(files_count)); 
		if (pattern_load(file_number) < 0)
		{
			return -1;
		}
		weight_change(iter);
	}

	return 0;
}

void destroy()
{
	table_destroy(top);
}

int initialize(char filename[])
{
	FILE* file;
	int res=0;
	int x,y;

	file = fopen(filename,"r");
	if (file == NULL)
	{
		fprintf(stderr, "File open failed");
		return 1;
	}
	for (; !feof(file) ;)
	{
		if (fscanf(file, "%d %d", &x, &y) < 2)
		{
			break;
		}
		top = table_push(top,x,y);
	}
	fclose(file);

	return res;
}
