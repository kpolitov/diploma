#ifndef TABLE_H
#define TABLE_H

typedef struct table
{
	int x;
	int y;
	struct table* link;
} table_t;

long table_get_count(table_t* table_top);
table_t* table_push(table_t* table_top, int x, int y);
void table_destroy(table_t* table_top);
void table_print(table_t* table_top);
table_t* table_get_record(table_t* table_top, int n);

#endif
