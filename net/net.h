#ifndef NNET_H
#define NNET_H

#define SIZE_LAYER_0 2
#define SIZE_LAYER_1 10

extern double layer_0[SIZE_LAYER_0];
extern double layer_1[SIZE_LAYER_1][SIZE_LAYER_1];
extern double weight[SIZE_LAYER_1][SIZE_LAYER_1][SIZE_LAYER_0];

void weight_init();
void weight_change();
int training();
void run();
int initialize(char filename[]);
void destroy();

#endif
