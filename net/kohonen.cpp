#include <stdio.h>
#include "net.h"

int main(int argc, char* argv[])
{
	int res=0;

	if (argc != 2)
	{
		fprintf(stderr,"Usage: %s <input file>\n", argv[0]);
		return 1;
	}
	
	if (initialize(argv[1]) != 0)
	{
		fprintf(stderr,"Initialization failed\n");
		return 1;
	}

	training();
	run();
	destroy();

	return res;
}
