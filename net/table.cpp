#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "table.h"

long table_get_count(table_t* table_top)
{
	long l;
	table_t* t;

	if ((t=table_top) == NULL)
	{
		return 0;
	}

	for (l=1; (t=t->link); l++);

	return l;
}

table_t* table_push(table_t* table_top, int x, int y)
{
	table_t* record;

	record = (table_t*)malloc(sizeof(table_t));

	if (record == NULL)
	{
		return NULL;
	}

	record->x = x;
	record->y = y;
	record->link = table_top;
	
	return record;
}

void table_destroy(table_t* table_top)
{
	table_t* record;

	while (table_top)
	{
		record = table_top;
		table_top = table_top->link;
		free(record);
	}
}

void table_print(table_t* table_top)
{
	table_t* record;

	for (record=table_top; record!=NULL; record=record->link)
	{
		fprintf(stdout,"%i %i\n",record->x,record->y);
	}
}

table_t* table_get_record(table_t* table_top, int n)
{
	table_t* record;
	int i=0;

	for (record=table_top, i=0; (record!=NULL) && (i<n); record=record->link, i++);

	return record;
}