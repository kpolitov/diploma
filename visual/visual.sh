#!/bin/sh

gnuplot_init="set terminal pngcairo;unset key;set xrange [0:100];set yrange [0:100]"

if [ ! -f  "../generator/generator" -o ! -f "../net/kohonen" ]; then
	echo "Build executables first"
	exit;
fi

rm -rf *.png *.txt

../generator/generator 500 > input.txt
../net/kohonen input.txt > output.txt

# Input

gnuplot -e "${gnuplot_init};set out \"input.png\";plot \"input.txt\" using 1:2 w p lc rgb 'black' pt 7 notitle;"

# Output

rm -rf output/*
while read line
do
	echo "${line#*:}" >> "output/${line%:*}.txt"
done < output.txt

gnuplot_plots=${gnuplot_init}";set out \"output.png\";set multiplot;"
plot_files=`find ./output/ -name "*.txt"`;

for plot in ${plot_files[@]}; do
	gnuplot_plots=${gnuplot_plots}"plot \"$plot\" using 1:2 w p lc rgb '#"`printf '%x' $(($RANDOM*$RANDOM/64))`"' pt 7 notitle;"
done

gnuplot -e "$gnuplot_plots"
