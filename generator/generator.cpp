#include <stdio.h>
#include <stdlib.h>
#include <time.h>

const int SQUARE_SIZE=100;

int main(int argc, char* argv[])
{  
	int x, y, count;

	if (argc > 1)
	{
		count = atoi(argv[1]);
	}
	else
	{
		count = 100;
	}

	if (count < 0)
	{
		return -1;
	}

	srand((unsigned int)time(NULL));

	for (int i=0; i<count; i++)
	{   
		x = (int)(((float)rand()/RAND_MAX)*(SQUARE_SIZE-1)); 
		y = (int)(((float)rand()/RAND_MAX)*(SQUARE_SIZE-1)); 
		printf("%i %i\n", x, y);
	}

	return 0;
}
